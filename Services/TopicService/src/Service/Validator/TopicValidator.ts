import { BaseValidator } from './BaseValidator';
import { DefaultValidatorType } from "./DefaultValidatorType";
import { StringPattern } from './Pattern/StringPattern';
import { Service } from "typedi";

@Service()
export class TopicValidator extends BaseValidator {

    public validateCreateData(requestData: any): void {
        super.validateData(requestData, [
            new DefaultValidatorType('code', new StringPattern(), true, 'code'),
            new DefaultValidatorType('libelle', new StringPattern(), true, 'libelle'),
            new DefaultValidatorType('badge', new StringPattern(), true, 'badge'),
        ]);
    }

    validateUpdateData(requestData: any) {
        super.validateData(requestData, [
            new DefaultValidatorType('code', new StringPattern(), false, 'code'),
            new DefaultValidatorType('libelle', new StringPattern(), false, 'libelle'),
            new DefaultValidatorType('badge', new StringPattern(), false, 'badge'),
        ]);
    }
}
