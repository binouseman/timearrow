import { Controller, Get } from 'routing-controllers';
import { AbstractController } from './AbstractController'
import { RoleService } from "../Service/RoleService";

@Controller('/mongo')
export class GetController extends AbstractController {

    @Get()
    public async getCrendential() {

        this.roleService.check(this.authenticatedUserAware.getAuthenticatedUser(), RoleService.USER);

        return this.credentialService.createCredential();
    }
}
