import { Credential } from '../Entity/Credential';
import { Inject, Service } from "typedi";
import { TokenGenerator} from 'ts-token-generator';
import MongoService from "./MongoService";

@Service()
export class CredentialService {

    @Inject()
    private mongoService: MongoService;


    public createCredential(): Credential {


        let username: string = new TokenGenerator().generate();
        let password: string = new TokenGenerator().generate();

        if (this.mongoService.addUser(username, password)) {

            setTimeout(() => {
               console.log('Needs to be removed');
            }, 10000);

            return (new Credential())
                .setUsername(username)
                .setPassword(password);
        }
    }
}
