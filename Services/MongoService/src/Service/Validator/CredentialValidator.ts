import { BaseValidator } from './BaseValidator';
import { Service } from "typedi";

@Service()
export class CredentialValidator extends BaseValidator {

    public validateCreateData(requestData: any): void {
        super.validateData(requestData, []);
    }

    validateUpdateData(requestData: any) {
        super.validateData(requestData, []);
    }
}
