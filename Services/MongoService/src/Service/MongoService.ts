import { Service } from 'typedi';
import { MongoClient } from 'mongodb';

@Service()
export default  class MongoService {

    public addUser(username: string, password: string): boolean {

        let mongoHost: string = process.env.MONGO_HOST;
        let mongoPort: string = process.env.MONGO_PORT;
        let dbName: string = process.env.MONGO_DBNAME;

        MongoClient.connect('mongodb://'+mongoHost+':'+mongoPort+'/'+dbName,
            function(err: any, db: any) {

                if (err){
                    return console.log('Error: could not connect to mongodb')
                }

                // console.log(MongoClient.admin());
                //
                // // Use the admin database for the operation
                // let adminDb = db.admin();
                //
                // // Add the new user to the admin database
                // adminDb.addUser(username, password, {
                //         roles:  [{
                //             role : "userAdmin",
                //             db   : dbName
                //         }]
                //     },
                //     function(err: any, result: any) {
                //
                //         if (err){
                //             return console.log('Error: could not add new user')
                //         }
                //
                //         // Authenticate using the newly added user
                //         adminDb.authenticate(username, password, function(err: any, result: any) {
                //
                //             if (err){
                //                 return console.log('Error: could authenticate with created user')
                //             }
                //
                //             db.close();
                //             return true;
                //         })
                //     })
            });

        return false;
    }

}
