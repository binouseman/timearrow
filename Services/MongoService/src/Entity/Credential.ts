import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";
import { DeleteAwareMixin } from "./Mixin/DeleteAwareMixin";

@Entity()
export class Credential extends DeleteAwareMixin {

    @PrimaryGeneratedColumn()
    private id: number;

    @Column({
        type: "varchar",
        default: null
    })
    private username: string;

    @Column({
        type: "varchar",
        default: null
    })
    private password: string;


    getId(): number {
        return this.id;
    }

    setId(value: number): this {
        this.id = value;
        return this;
    }

    getUsername(): string {
        return this.username;
    }

    setUsername(value: string): this {
        this.username = value;
        return this;
    }

    getPassword(): string {
        return this.password;
    }

    setPassword(value: string): this {
        this.password = value;
        return this;
    }
}

