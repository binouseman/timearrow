import * as express from 'express';
import { ExpressErrorMiddlewareInterface, HttpError, Middleware } from 'routing-controllers';
import { Inject } from "typedi";
import {TimelineRepository} from "../Repository/TimelineRepository";

@Middleware({ type: 'after' })
export class ErrorHandlerMiddleware implements ExpressErrorMiddlewareInterface {

    @Inject()
    protected timelineRepository: TimelineRepository;

    error(error: HttpError, req: express.Request, res: express.Response, next: express.NextFunction): void {

        this.timelineRepository.connection.close();

        if (!res.headersSent) {
            res.status(error.httpCode || 500);

            res.json({
                message: error.message
            });
        }
    }
}
