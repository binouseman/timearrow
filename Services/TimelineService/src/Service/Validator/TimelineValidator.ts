import { BaseValidator } from './BaseValidator';
import { DefaultValidatorType } from "./DefaultValidatorType";
import { StringPattern } from './Pattern/StringPattern';
import { Service } from "typedi";
import {ArrayPattern} from "./Pattern/ArrayPattern";

@Service()
export class TimelineValidator extends BaseValidator {

    public validateCreateData(requestData: any): void {
        super.validateData(requestData, [
            new DefaultValidatorType('title', new StringPattern(), true, 'title'),
            new DefaultValidatorType('from', new StringPattern(), true, 'from'),
            new DefaultValidatorType('to', new StringPattern(), true, 'to'),
            new DefaultValidatorType('resume', new StringPattern(), true, 'resume'),
            new DefaultValidatorType('topics', new ArrayPattern(), true, 'topics'),
            new DefaultValidatorType('events', new ArrayPattern(), true, 'events'),
        ]);
    }

    validateUpdateData(requestData: any) {
        super.validateData(requestData, [
            new DefaultValidatorType('title', new StringPattern(), false, 'title'),
            new DefaultValidatorType('from', new StringPattern(), false, 'from'),
            new DefaultValidatorType('to', new StringPattern(), false, 'to'),
            new DefaultValidatorType('resume', new StringPattern(), false, 'resume'),
            new DefaultValidatorType('topics', new ArrayPattern(), false, 'topics'),
            new DefaultValidatorType('events', new ArrayPattern(), false, 'events'),
        ]);
    }
}
