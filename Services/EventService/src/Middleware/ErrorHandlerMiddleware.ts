import * as express from 'express';
import { ExpressErrorMiddlewareInterface, HttpError, Middleware } from 'routing-controllers';
import { Inject } from "typedi";
import { EventRepository } from "../Repository/EventRepository";

@Middleware({ type: 'after' })
export class ErrorHandlerMiddleware implements ExpressErrorMiddlewareInterface {

    @Inject()
    protected eventRepository: EventRepository;

    error(error: HttpError, req: express.Request, res: express.Response, next: express.NextFunction): void {

        this.eventRepository.connection.close();

        if (!res.headersSent) {
            res.status(error.httpCode || 500);

            res.json({
                message: error.message
            });
        }
    }
}
