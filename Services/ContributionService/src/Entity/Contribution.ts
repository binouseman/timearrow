import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";
import { DeleteAwareMixin } from "./Mixin/DeleteAwareMixin";

@Entity()
export class Contribution extends DeleteAwareMixin {

    @PrimaryGeneratedColumn()
    private id: number;

    @Column({
        type: "int",
        default: null
    })
    private state: number;

    @Column({
        type: "int",
        default: null
    })
    private timeline: number;

    @Column({
        type: "int",
        default: null
    })
    private eventBefore: number;

    @Column({
        type: "int",
        default: null
    })
    private eventAfter: number;

    @Column({
        type: "int",
        default: null
    })
    private user: number;

    @Column({
        type: "datetime",
        default: null
    })
    private date: Date;


    getId(): number {
        return this.id;
    }

    setId(value: number) {
        this.id = value;
    }

    getState(): number {
        return this.state;
    }

    setState(value: number): Contribution {
        this.state = value;
        return  this;
    }

    getTimeline(): number {
        return this.timeline;
    }

    setTimeline(value: number): Contribution{
        this.timeline = value;
        return this;
    }

    getEventBefore(): number {
        return this.eventBefore;
    }

    setEventBefore(value: number): Contribution {
        this.eventBefore = value;
        return this;
    }

    getEventAfter(): number {
        return this.eventAfter;
    }

    setEventAfter(value: number): Contribution {
        this.eventAfter = value;
        return this;
    }

    getUser(): number {
        return this.user;
    }

    setUser(value: number): Contribution {
        this.user = value;
        return this;
    }

    getDate(): Date {
        return this.date;
    }

    setDate(value: Date): this {
        this.date = value;
        return this;
    }
}

