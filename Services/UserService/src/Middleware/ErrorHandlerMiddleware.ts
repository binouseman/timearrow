import * as express from 'express';
import { ExpressErrorMiddlewareInterface, HttpError, Middleware } from 'routing-controllers';
import { Inject } from "typedi";
import { UserRepository } from "../Repository/UserRepository";

@Middleware({ type: 'after' })
export class ErrorHandlerMiddleware implements ExpressErrorMiddlewareInterface {

    @Inject()
    protected userRepository: UserRepository;

    error(error: HttpError, req: express.Request, res: express.Response, next: express.NextFunction): void {

        this.userRepository.connection.close();

        if (!res.headersSent) {
            res.status(error.httpCode || 500);

            res.json({
                message: error.message
            });
        }
    }
}
